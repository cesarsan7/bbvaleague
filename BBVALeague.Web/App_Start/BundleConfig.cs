﻿using System.Web;
using System.Web.Optimization;

namespace BBVALeague.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
						
						
			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/custom-script.js",
                      "~/Scripts/materialize.js",
					 "~/Scripts/materialize.min.js",
					 "~/Scripts/plugins.js",
					 "~/Scripts/plugins.min.js"
					
					));
			
             /*
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
					"~/Scripts/plugins/DataTables/Buttons-1.1.0/js/dataTables.buttons.min.js"));
			*/
             /*
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"
					  
					  ));
			*/
			bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/custom/custom.css",
					  "~/Content/custom/custom.min.css" ,
					  "~/Content/layaout/layout-2.min.css" ,
					  "~/Content/layaout/layout-3.min.css" ,
					  "~/Content/layaout/page-center.css" ,
					  "~/Content/layaout/style-fullscreen.css" ,
					  "~/Content/layaout/style-horizontal.css" ,					  
					   "~/Content/plugins/media-hover-effects.css" ,
                      "~/Content/materialize.css",					  
					  "~/Content/materialize.min.css",
					  "~/Content/style.css",
					   "~/Content/style.min.css"
					  
					  ));
			
        }
    }
}
