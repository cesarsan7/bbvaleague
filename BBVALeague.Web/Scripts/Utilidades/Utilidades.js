﻿/*mensaje=texto del mensaje que se va a mostrar
         tipoMensaje= 1 indica un mensaje correcto (Felicidades), 0 indica un mensaje de error
         limpiarMensajeAnterior=true para eliminar el mensaje anterior mostrado
         mensajeAdicional=Si hay un mensaje adicional para el mesaje de felicidades
         control=indica el control donde se va a mostrar el mensaje
         */
function UtilidadMostrarMensaje(mensaje, tipoMensaje, limpiarMensajeAnterior, mensajeAdicional, control) {

    if (control == null) {
        if (limpiarMensajeAnterior == true)
            $('#div_Mensaje').remove();

        if (tipoMensaje == 1) {
            if (mensajeAdicional == null)
                $('#mensaje').append(
                   '<div class="alert alert-success fade in" id="div_Mensaje"> ' +
                   '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>' +
                   ' <p><img src="/Images/yes.png" alt="ok">&nbsp;&nbsp;' + mensaje + '</p> </div>');
                //<h4>Felicitaciones!.</h4>
            else
                $('#mensaje').append(
                   '<div class="alert alert-success fade in" id="div_Mensaje"> ' +
                   '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>' +
                   ' <p><img src="/Images/yes.png" alt="ok">&nbsp;&nbsp;' + mensaje + '</p>' +
                   mensajeAdicional + '</div>');
            //<h4>Felicitaciones!.</h4>       
        }
        else {
            $('#mensaje').append(
                '<div id="div_Mensaje" class="alert alert-danger">' +
                '<button type="button" class="close" data-dismiss="alert">' +
                '&times;</button><b>Error! </b>' + mensaje + '</div>');
        }
    }
    else {
        if (tipoMensaje == 1) {
            if (limpiarMensajeAnterior == true)
                $(control).html('');
            if (mensajeAdicional == null)
                $(control).append(
                   '<div class="alert alert-success fade in" id="div_Mensaje2"> ' +
                   '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>' +
                   ' <p><img src="/Images/yes.png" alt="ok">&nbsp;&nbsp;' + mensaje + '</p> </div>');
                //<h4>Felicitaciones!.</h4>    
            else
                $(control).append(
                   '<div class="alert alert-success fade in" id="div_Mensaje2"> ' +
                   '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>' +
                   '<p><img src="/Images/yes.png" alt="ok">&nbsp;&nbsp;' + mensaje + '</p>' +
                   mensajeAdicional + '</div>');
            //<h4>Felicitaciones!.</h4>        
        }
        else {
            $(control).append(
                '<div id="div_Mensaje2" class="alert alert-danger">' +
                '<button type="button" class="close" data-dismiss="alert">' +
                '&times;</button><b>Error! </b>' + mensaje + '</div>');
        }
    }

}
/*Valida el ingreso de numeros naturales*/
function UtilidadSoloNumeros(input) {
    $(input).bind("keypress", function (e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57));
        return ret;
    });

    $(input).bind("paste", function (e) {
        return false;
    });
    $(input).bind("drop", function (e) {
        return false;
    });

}

/*Valida el ingreso de numeros naturales entre un rango*/
function UtilidadSoloNumerosRango(input,a,b) {
    $(input).bind("keypress", function (e) {
        var keyCode = e.which ? e.which : e.keyCode
        var inicia = (a === 0) ? 48 : (a === 1) ? 49 : (a === 2) ? 50 : (a === 3) ? 51 : (a === 4) ? 52 : (a === 5) ? 53 : (a === 6) ? 54 : (a === 7) ? 55 : (a === 8) ? 56 : 57;
        var fin    = (b === 0) ? 48 : (b === 1) ? 49 : (b === 2) ? 50 : (b === 3) ? 51 : (b === 4) ? 52 : (b === 5) ? 53 : (b === 6) ? 54 : (b === 7) ? 55 : (b === 8) ? 56 : 57;
        var ret = ((keyCode >= inicia && keyCode <= fin));        
        return ret;
    });

    $(input).bind("paste", function (e) {
        return false;
    });
    $(input).bind("drop", function (e) {
        return false;
    });

}

/*Valida el ingreso de numeros decimales*/
function UtilidadSoloDecimales(input) {

    $(input).bind("keypress", function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode;
        /*if (charCode == 46 && evt.srcElement.value.split('.').length > 1) {
            return false;
        }*/
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    });
    $(input).bind("paste", function (e) {
        return false;
    });
    $(input).bind("drop", function (e) {
        return false;
    });
}

/*Evento del control para volver a la pagina anterior*/
$('#volver').click(function () {
    history.back();
    return false;
});

/*Carga el listado de servicios*/
function UtilidadListadoServicios(ddl, listado, CallBack) {
    if (listado == null)
        listado = false;

    var parametros = { listaMaestra: listado }
    $.post('/TablaValor/ObtenerServicios', parametros, function (data) {
        ddl.html("");
        $(data).each(function (i, item) {
            ddl.append('<option value="' + item.ID + '">' + item.Nombre + '</option>');
        });
        if (CallBack != null)
            CallBack();
    });
}

/*Carga el listado de servicios*/
function UtilidadListadoServicios(ddl, listado, CallBack, tituloCabecera) {
    if (listado == null)
        listado = false;
    if (tituloCabecera == null)
        tituloCabecera="[Todos]"
    var parametros = { listaMaestra: listado,tituloCabecera:tituloCabecera }
    $.post('/TablaValor/ObtenerServicios', parametros, function (data) {
        ddl.html("");
        $(data).each(function (i, item) {
            ddl.append('<option value="' + item.ID + '">' + item.Nombre + '</option>');
        });
        if (CallBack != null)
            CallBack();
    });
}

/*Valida si un registro esta contenido dentro de los datos de un table, retorna true si se encuentra en los registros
         tTabla: DataTable(), con los datos donde se buscara el valor si se encuentra en esa lista,
         posicion:Posicion en la tabla donde se buscara el valor
         valor:Valor a buscar
         */
function UtilidadExisteRegistroDataTable(tTabla, posicion, valor) {

    var existeRegistro = false;
    $(tTabla.rows().data()).each(function (i, item) {
        if (item[posicion] == valor) {
            existeRegistro = true;
            return false;
        }
    });
    return existeRegistro;

}

/*
Funcion que muestra un mensaje al lado derecho del control input como tooltip
input:Control al mostrar el Tooltip
mensaje:Mensaje que se despliega el tooltip
*/
function UtilidadAsignarTooltipDerecha(input, mensaje) {
    $(input).on("focus", function () {

        $(input).attr('data-content', mensaje);
        $(input).attr('data-placement', 'right');
        $(input).attr('data-toggle', 'popover');
        $(input).attr('data-trigger', 'focus');

        $(input).popover('show');

    });
}

/*Funcion que retorna un valor booleano si la fecha es correcta y cumple con el formato dd/mm/yyyy*/
function UtilidadValidarFechaDDMMYYYY(fecha) {
    var formatoCorreto = /^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}$/.test(fecha);
    if (formatoCorreto)
        return UtilidadFechaCorrecta(fecha.substring(3, 5), fecha.substring(0, 2), fecha.substring(6, 10))
    else
        return formatoCorreto;
}
/*Funcion que valida el formato de una fecha*/

function UtilidadValidarFormatoFecha(fecha) {
   return /^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}$/.test(fecha);
}
/*Valida si la fecha registrada es una fecha valida*/
function UtilidadFechaCorrecta(m, d, y) {
    return m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= (new Date(y, m, 0))
      .getDate();
}

/*Convierte la fecha de Json a Date*/
function UtilidadformatJSONDate(jsonDate) {

    var newDate = new Date(parseInt(jsonDate.substr(6)));
    return newDate;
}
//Valida que el correo sea correcto
function ValidateEmail(email) {

    var expr = /^([\w-\.]+)@@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};