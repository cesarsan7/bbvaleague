/*
 * 	 imCheckBoxDB - a JQuery Plugin
 * 	 @author Les Green
 * 	 Copyright (C) 2008 Intriguing Minds, Inc.
 *   Version 0.5
 * 
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.

 *   Demo and Documentation can be found at:   
 *   http://www.grasshopperpebbles.com
 *   
 */

; (function ($) {
    $.fn.extend({
        imCheckBoxDB: function (options) {
            opts = $.extend({}, $.dbCheckBox.defaults, options);
            return this.each(function () {
                new $.dbCheckBox(this, opts);
            });
        }
    });


    $.dbCheckBox = function (obj, opts) {
        var $this = $(obj);
        if (opts.db_map != '') {
            var d = getDataString();
            doAjax('GET', opts.data_url, d, '', doCreate);
        }

        function getDataString() {
            var str = '';
            $.each(opts.data, function (i, itm) {
                str += itm.name + "=" + itm.value + "&";
            });
            //remove last "&"
            str = str.substr(0, (str.length - 1));
            return str;
        };

        function doAjax(t, u, d, fnBefore, fnSuccess) {
            $.ajax({
                type: t,
                url: u,
                data: d,
                dataType: 'json',
                beforeSend: fnBefore, //function(){$("#loading").show("fast");}, //show loading just when link is clicked
                //complete: function(){ $("#loading").hide("fast");}, //stop showing loading when the process is complete
                success: fnSuccess
            }); //close $.ajax(
        };

        function doCreate(data) {
            //var tbl, tr;
            var nRecs = data.length;
            var nRows = Math.ceil(nRecs / opts.num_cols);
            cnt = 0;
            var tbl = document.createElement('table');
            $(tbl).attr({ width: "100%", border: "0" });
            for (var i = 0; i < nRows; i++) {
                var tr = document.createElement('tr');
                for (var j = 0; j < opts.num_cols; j++) {
                    var td = document.createElement('td');
                    //console.log(data[cnt][opts.db_map.label]);
                    //$(td).append($('<input type="checkbox" name="' + opts.cb_prefix + data[cnt][opts.db_map.id] + '" value="' + data[cnt][opts.db_map.id] + '" />').addClass(opts.cb_class));
                    $(td).append($('<input style="margin:10px; padding:40px;  height:18px;  width:18px" grupo="group" type="checkbox" name="' + opts.cb_prefix + data[cnt][opts.db_map.id] + '" value="' + data[cnt][opts.db_map.id] + '" />'));
                    $(td).append(data[cnt][opts.db_map.label]);
                    cnt++;
                    $(tr).append(td);
                    if (cnt == nRecs) {
                        break;
                    }
                }
                $(tbl).append(tr);
            }
            $this.append($(tbl));
            if (opts.CallBack != null)
                opts.CallBack();
        };
    };

    $.dbCheckBox.defaults = {
        /*data_url: '../includes/controllers/dbActionController.php',
        data: [{name: "action", value: "doGetJson"}, {name: "page", value: "music_categories"} ],
        data_map: {"id": "music_cat_id", "label" : "music_desc"},
        cb_class : 'checkbox',
        num_cols: 4*/
        data_url: '',
        data: '',
        db_map: '',
        cb_class: '',
        cb_prefix: 'chb_',
        num_cols: 4,
        CallBack: null
    };
})(jQuery);