/// <autosync enabled="true" />
/// <reference path="bootstrap.js" />
/// <reference path="chart/chart.js" />
/// <reference path="custom-script.js" />
/// <reference path="file-upload/app.js" />
/// <reference path="file-upload/cors/jquery.postmessage-transport.js" />
/// <reference path="file-upload/cors/jquery.xdr-transport.js" />
/// <reference path="file-upload/jquery.fileupload.js" />
/// <reference path="file-upload/jquery.fileupload-angular.js" />
/// <reference path="file-upload/jquery.fileupload-audio.js" />
/// <reference path="file-upload/jquery.fileupload-image.js" />
/// <reference path="file-upload/jquery.fileupload-jquery-ui.js" />
/// <reference path="file-upload/jquery.fileupload-process.js" />
/// <reference path="file-upload/jquery.fileupload-ui.js" />
/// <reference path="file-upload/jquery.fileupload-validate.js" />
/// <reference path="file-upload/jquery.fileupload-video.js" />
/// <reference path="file-upload/jquery.iframe-transport.js" />
/// <reference path="file-upload/main.js" />
/// <reference path="file-upload/vendor/jquery.ui.widget.js" />
/// <reference path="icheck/demo/js/custom.min.js" />
/// <reference path="icheck/demo/js/jquery.js" />
/// <reference path="icheck/demo/js/zepto.js" />
/// <reference path="icheck/icheck.js" />
/// <reference path="jqplot/excanvas.js" />
/// <reference path="jqplot/jqplot.axislabelrenderer.js" />
/// <reference path="jqplot/jqplot.axistickrenderer.js" />
/// <reference path="jqplot/jqplot.canvasgridrenderer.js" />
/// <reference path="jqplot/jqplot.core.js" />
/// <reference path="jqplot/jqplot.divtitlerenderer.js" />
/// <reference path="jqplot/jqplot.donutrenderer.js" />
/// <reference path="jqplot/jqplot.effects.blind.js" />
/// <reference path="jqplot/jqplot.effects.core.js" />
/// <reference path="jqplot/jqplot.linearaxisrenderer.js" />
/// <reference path="jqplot/jqplot.lineartickgenerator.js" />
/// <reference path="jqplot/jqplot.linepattern.js" />
/// <reference path="jqplot/jqplot.linerenderer.js" />
/// <reference path="jqplot/jqplot.markerrenderer.js" />
/// <reference path="jqplot/jqplot.pierenderer.js" />
/// <reference path="jqplot/jqplot.shadowrenderer.js" />
/// <reference path="jqplot/jqplot.shaperenderer.js" />
/// <reference path="jqplot/jqplot.sprintf.js" />
/// <reference path="jqplot/jqplot.tablelegendrenderer.js" />
/// <reference path="jqplot/jqplot.themeengine.js" />
/// <reference path="jqplot/jqplot.toimage.js" />
/// <reference path="jqplot/jquery.jqplot.js" />
/// <reference path="jqplot/jquery.js" />
/// <reference path="jqplot/jsdate.js" />
/// <reference path="jqplot/plugins/jqplot.barrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.beziercurverenderer.js" />
/// <reference path="jqplot/plugins/jqplot.blockrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.bubblerenderer.js" />
/// <reference path="jqplot/plugins/jqplot.canvasaxislabelrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.canvasaxistickrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.canvasoverlay.js" />
/// <reference path="jqplot/plugins/jqplot.canvastextrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.categoryaxisrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.ciparser.js" />
/// <reference path="jqplot/plugins/jqplot.cursor.js" />
/// <reference path="jqplot/plugins/jqplot.dateaxisrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.donutrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.dragable.js" />
/// <reference path="jqplot/plugins/jqplot.enhancedlegendrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.funnelrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.highlighter.js" />
/// <reference path="jqplot/plugins/jqplot.json2.js" />
/// <reference path="jqplot/plugins/jqplot.logaxisrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.mekkoaxisrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.mekkorenderer.js" />
/// <reference path="jqplot/plugins/jqplot.metergaugerenderer.js" />
/// <reference path="jqplot/plugins/jqplot.mobile.js" />
/// <reference path="jqplot/plugins/jqplot.ohlcrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.pierenderer.js" />
/// <reference path="jqplot/plugins/jqplot.pointlabels.js" />
/// <reference path="jqplot/plugins/jqplot.pyramidaxisrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.pyramidgridrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.pyramidrenderer.js" />
/// <reference path="jqplot/plugins/jqplot.trendline.js" />
/// <reference path="jquery.imcheckboxdb-0.5.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.11.1.js" />
/// <reference path="jquery-ui.js" />
/// <reference path="jquery-ui/jquery-ui.js" />
/// <reference path="materialize.min.js" />
/// <reference path="materialize-plugins/animation.js" />
/// <reference path="materialize-plugins/buttons.js" />
/// <reference path="materialize-plugins/cards.js" />
/// <reference path="materialize-plugins/carousel.js" />
/// <reference path="materialize-plugins/character_counter.js" />
/// <reference path="materialize-plugins/chips.js" />
/// <reference path="materialize-plugins/collapsible.js" />
/// <reference path="materialize-plugins/date_picker/picker.date.js" />
/// <reference path="materialize-plugins/date_picker/picker.js" />
/// <reference path="materialize-plugins/dropdown.js" />
/// <reference path="materialize-plugins/forms.js" />
/// <reference path="materialize-plugins/global.js" />
/// <reference path="materialize-plugins/hammer.min.js" />
/// <reference path="materialize-plugins/jquery.easing.1.3.js" />
/// <reference path="materialize-plugins/jquery.hammer.js" />
/// <reference path="materialize-plugins/jquery.timeago.min.js" />
/// <reference path="materialize-plugins/leanmodal.js" />
/// <reference path="materialize-plugins/materialbox.js" />
/// <reference path="materialize-plugins/materialize.min.js" />
/// <reference path="materialize-plugins/parallax.js" />
/// <reference path="materialize-plugins/prism.js" />
/// <reference path="materialize-plugins/pushpin.js" />
/// <reference path="materialize-plugins/scrollfire.js" />
/// <reference path="materialize-plugins/scrollspy.js" />
/// <reference path="materialize-plugins/sidenav.js" />
/// <reference path="materialize-plugins/slider.js" />
/// <reference path="materialize-plugins/tabs.js" />
/// <reference path="materialize-plugins/toasts.js" />
/// <reference path="materialize-plugins/tooltip.js" />
/// <reference path="materialize-plugins/transitions.js" />
/// <reference path="materialize-plugins/velocity.min.js" />
/// <reference path="materialize-plugins/waves.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="morris/gruntfile.js" />
/// <reference path="morris/morris.js" />
/// <reference path="morris/raphael-min.js" />
/// <reference path="morris/spec/viz/examples.js" />
/// <reference path="morris/spec/viz/visual_specs.js" />
/// <reference path="plugins.min.js" />
/// <reference path="plugins/chartist-js/chartist.js" />
/// <reference path="plugins/chartist-js/chartist-script.js" />
/// <reference path="plugins/chartjs/chart.min.js" />
/// <reference path="plugins/chartjs/chartjs-sample-chart.js" />
/// <reference path="plugins/chartjs/chart-script.js" />
/// <reference path="plugins/DataTables/AutoFill-2.1.0/js/autoFill.bootstrap.js" />
/// <reference path="plugins/DataTables/AutoFill-2.1.0/js/autoFill.foundation.js" />
/// <reference path="plugins/DataTables/AutoFill-2.1.0/js/autoFill.jqueryui.js" />
/// <reference path="plugins/DataTables/AutoFill-2.1.0/js/dataTables.autoFill.js" />
/// <reference path="plugins/DataTables/Buttons-1.1.0/js/buttons.bootstrap.js" />
/// <reference path="plugins/DataTables/Buttons-1.1.0/js/buttons.colVis.js" />
/// <reference path="plugins/DataTables/Buttons-1.1.0/js/buttons.flash.js" />
/// <reference path="plugins/DataTables/Buttons-1.1.0/js/buttons.foundation.js" />
/// <reference path="plugins/DataTables/Buttons-1.1.0/js/buttons.html5.js" />
/// <reference path="plugins/DataTables/Buttons-1.1.0/js/buttons.jqueryui.js" />
/// <reference path="plugins/DataTables/Buttons-1.1.0/js/buttons.print.js" />
/// <reference path="plugins/DataTables/Buttons-1.1.0/js/dataTables.buttons.js" />
/// <reference path="plugins/DataTables/ColReorder-1.3.0/js/dataTables.colReorder.js" />
/// <reference path="plugins/DataTables/datatables.js" />
/// <reference path="plugins/DataTables/DataTables-1.10.10/js/dataTables.bootstrap.js" />
/// <reference path="plugins/DataTables/DataTables-1.10.10/js/dataTables.foundation.js" />
/// <reference path="plugins/DataTables/DataTables-1.10.10/js/dataTables.jqueryui.js" />
/// <reference path="plugins/DataTables/DataTables-1.10.10/js/jquery.dataTables.js" />
/// <reference path="plugins/DataTables/FixedColumns-3.2.0/js/dataTables.fixedColumns.js" />
/// <reference path="plugins/DataTables/FixedHeader-3.1.0/js/dataTables.fixedHeader.js" />
/// <reference path="plugins/DataTables/KeyTable-2.1.0/js/dataTables.keyTable.js" />
/// <reference path="plugins/DataTables/Responsive-2.0.0/js/dataTables.responsive.js" />
/// <reference path="plugins/DataTables/Responsive-2.0.0/js/responsive.bootstrap.js" />
/// <reference path="plugins/DataTables/Responsive-2.0.0/js/responsive.foundation.js" />
/// <reference path="plugins/DataTables/Responsive-2.0.0/js/responsive.jqueryui.js" />
/// <reference path="plugins/DataTables/RowReorder-1.1.0/js/dataTables.rowReorder.js" />
/// <reference path="plugins/DataTables/Scroller-1.4.0/js/dataTables.scroller.js" />
/// <reference path="plugins/DataTables/Select-1.1.0/js/dataTables.select.js" />
/// <reference path="plugins/google-map/google-map-script.js" />
/// <reference path="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" />
/// <reference path="plugins/jvectormap/jquery-jvectormap-world-mill-en.js" />
/// <reference path="plugins/jvectormap/vectormap-script.js" />
/// <reference path="plugins/metismenu/metismenu.js" />
/// <reference path="plugins/perfect-scrollbar/perfect-scrollbar.min.js" />
/// <reference path="plugins/sparkline/jquery.sparkline.min.js" />
/// <reference path="plugins/sparkline/sparkline-script.js" />
/// <reference path="respond.js" />
/// <reference path="utilidades/utilidades.js" />
