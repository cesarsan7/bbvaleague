﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BBVALeague.Web.Models
{
    public class LoginViewModel
    {

        [DataType(DataType.Password)]
        public string email { get; set; }


        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string password { get; set; }

    }
}