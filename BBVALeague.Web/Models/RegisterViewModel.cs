﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BBVALeague.Web.Models
{
    public class RegisterViewModel
    {

        [Key]
        public long idUser { get; set; }

        [Required(ErrorMessage = "name is required")]
        public string name { get; set; }

        [Required(ErrorMessage = "Email is required")]
        public string email { get; set; }


        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [Compare("password",ErrorMessage ="Please confirm your password")]
        [DataType(DataType.Password)]
        public string confirmPassword { get; set; }




    }
}