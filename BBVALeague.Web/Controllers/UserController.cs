﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BBVALeague.Dal.EntityModel;
using BBVALeague.Web.Models;
using System.Web.Security;

namespace BBVALeague.Web.Controllers
{
    public class UserController : Controller
    {
        private DeveloperTestEntities db = new DeveloperTestEntities();

        // GET: User
        [Authorize]
        public ActionResult Index()
        {
            return View(db.USERS.ToList());
        }


        public ActionResult login()
        {
            return View();
        }

    
        [HttpPost]
        public ActionResult login(LoginViewModel user )
        {         
            var objUser = (from tabla in db.USERS
                           where tabla.email == user.email && tabla.password == user.password
                           select tabla
                        );


            if (objUser.Any())
            {
                ViewBag.Error = "Usuario correcto";
                FormsAuthentication.SetAuthCookie(user.email, false);
                return RedirectToAction("Index", "Team");
            }
            else
            {
                ModelState.AddModelError("Error", "UserName or Password is Wrong");
                ViewBag.Error = "UserName or Password is Wrong";

            }

            return View();
        }


        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }


        // GET: User/Details/5
        [Authorize]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USERS uSERS = db.USERS.Find(id);
            if (uSERS == null)
            {
                return HttpNotFound();
            }
            return View(uSERS);
        }

        // GET: User/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            return View();
        }

        // GET: User/Create
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }


        // POST: User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idUser,email,password,name,confirmPassword")] USERS uSERS)
        {
            if (ModelState.IsValid)
            {
                db.USERS.Add(uSERS);
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }

            return View(uSERS);
        }

        // GET: User/Edit/5
        [Authorize]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USERS uSERS = db.USERS.Find(id);
            if (uSERS == null)
            {
                return HttpNotFound();
            }
            return View(uSERS);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idUser,email,password,name,confirmPassword")] USERS uSERS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uSERS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(uSERS);
        }

        // GET: User/Delete/5
        [Authorize]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USERS uSERS = db.USERS.Find(id);
            if (uSERS == null)
            {
                return HttpNotFound();
            }
            return View(uSERS);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            USERS uSERS = db.USERS.Find(id);
            db.USERS.Remove(uSERS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
