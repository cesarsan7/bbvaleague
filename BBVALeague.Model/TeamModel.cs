﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBVALeague.Model
{
    public class TeamModel
    {
		public int idTeam { set; get; }
		public string nameTeam { set; get; }
    }
}
