﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBVALeague.Model
{
    public partial class UserModel
    {
        public long idUser { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string name { get; set; }
    }
}
