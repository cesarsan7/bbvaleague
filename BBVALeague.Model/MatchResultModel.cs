﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBVALeague.Model
{
	public class MatchResultModel
	{
		public long idFixture { get; set; }
		public string LocalTeam { get; set; }
		public string VisistorTeam   { get ; set ;}
		public int localGoals { get; set; }
		public int visitorGoals { get; set; }
		public DateTime playDate { get; set; }
		public string monthName { get; set; }
		public int ? idLocalTeam { get; set; }
		public int ? idvisitorTeam { get; set; }
		public int ? idMonth { get; set; }
		public int ? year  { get; set; }
   }
}
