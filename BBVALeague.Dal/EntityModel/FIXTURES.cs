//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BBVALeague.Dal.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class FIXTURES
    {
        public long idFixture { get; set; }
        public string season { get; set; }
        public string division { get; set; }
        public string jornada { get; set; }
        public string localTeam { get; set; }
        public string visitorTeam { get; set; }
        public int localGoals { get; set; }
        public int visitorGoals { get; set; }
        public System.DateTime playDate { get; set; }
        public long timestamp { get; set; }
        public Nullable<int> year { get; set; }
        public Nullable<int> idLocalTeam { get; set; }
        public Nullable<int> idvisitorTeam { get; set; }
        public Nullable<int> idMonth { get; set; }
        public Nullable<int> idSeason { get; set; }
    
        public virtual TEAM TEAM { get; set; }
        public virtual MONTH MONTH { get; set; }
        public virtual SEASON SEASON1 { get; set; }
        public virtual TEAM TEAM1 { get; set; }
    }
}
