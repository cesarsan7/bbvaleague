﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using BBVALeague.WepApi.Handler;
using System.Web.Http.Cors;

namespace BBVALeague.WepApi
{
	public static class WebApiConfig
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="config"></param>
		public static void Register(HttpConfiguration config)
		{
			// Web API configuration and services

			// Web API routes

			
			config.MapHttpAttributeRoutes();
            var cors = new EnableCorsAttribute(origins: "*", headers: "*", methods: "*");
            config.EnableCors(cors);
            config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);

			config.Routes.MapHttpRoute(
			   name: "DefaultApi2",
			   routeTemplate: "api/{controller}/{id}/{year}/{month}",
			   defaults: new { month = RouteParameter.Optional }
		   );

			config.MessageHandlers.Add(new ValidationHandler());
		}
	}
}
