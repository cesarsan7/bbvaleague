﻿using BBVALeague.Common;
using BBVALeague.Dal.EntityModel;
using BBVALeague.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Caching;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BBVALeague.WepApi.Controllers
{
    
    public class TeamController : ApiController
    {
		private DeveloperTestEntities db = new DeveloperTestEntities();
		private MemoryCache memoryCache = MemoryCache.Default;

        /// <summary>
        /// Allow to return to the list of TeamModel 
        /// </summary>
        /// <returns>list objets TeamModel </returns>
        
        public IEnumerable<TeamModel> GetTeam()
		{
			var objCache = memoryCache.Get("GetTeam");

			if(objCache!=null )
			{
				Log4NetHelper.GetLog().AddLog(LogLevel.Info, "get data data Team from cache. ");
				return objCache as List<TeamModel>;
			}
			else
			{
				var lst = (from item in db.TEAM
						   select new TeamModel
						   {
							   idTeam = item.idTeam,
							   nameTeam = item.teamName
						   }
					  ).ToList<TeamModel>();

				memoryCache.Add("GetTeam", lst, DateTimeOffset.UtcNow.AddMinutes(5));
				Log4NetHelper.GetLog().AddLog(LogLevel.Info, "Caching data Team. ");
				return lst;
			}
			
		}

	}
}
