﻿using BBVALeague.Common;
using BBVALeague.Dal.EntityModel;
using BBVALeague.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Caching;
using System.Web.Http;

namespace BBVALeague.WepApi.Controllers
{
    public class MonthController : ApiController
    {
		private DeveloperTestEntities db = new DeveloperTestEntities();
		private MemoryCache memoryCache = MemoryCache.Default;
		/// <summary>
		///   	Allow to return to the list of months
		/// </summary>
		/// <returns>list objets MonthModel</returns>
		public IEnumerable<MonthModel> GetMonths()
		{
			var objCache = memoryCache.Get("GetMonths");
			
			if (objCache != null)
			{
				Log4NetHelper.GetLog().AddLog(LogLevel.Info, "get data data Month from cache. ");
				return objCache as List<MonthModel>;
			}
			else
			{	
				var lst = (from item in db.MONTH
						   select new MonthModel
						   {
							   idMonth = item.idMonth,
							   monthName = item.monthName
						   }
						).ToList<MonthModel>();
				Log4NetHelper.GetLog().AddLog(LogLevel.Info, "Caching data Month. ");
				memoryCache.Add("GetMonths", lst, DateTimeOffset.UtcNow.AddMinutes(5));
				return lst;
			}
			
		}
	}
}
