﻿using BBVALeague.Common;
using BBVALeague.Dal.EntityModel;
using BBVALeague.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Caching;
using System.Web.Http;

namespace BBVALeague.WepApi.Controllers
{
	public class MatchResultsController : ApiController
	{
		private DeveloperTestEntities db = new DeveloperTestEntities();
		private MemoryCache memoryCache = MemoryCache.Default;
		/// <summary>
		/// get the last five results the match the teams 
		/// </summary>
		/// <returns>List of the objet MatchResultModel </returns>
		public IEnumerable<MatchResultModel> GetLastMatchResults()
		{

			var objCache = memoryCache.Get("GetAllMatchResults1");

			if (objCache != null)
			{

				Log4NetHelper.GetLog().AddLog(LogLevel.Info, "get data data GetAllMatchResults() from cache.");
				return objCache as List<MatchResultModel>;
			}
			else
			{

				var lst = (from item in db.FixturesScores
						   select new MatchResultModel
						   {
							   idFixture = item.idFixture,
							   LocalTeam = item.LocalTeam,
							   VisistorTeam = item.VisistorTeam,
							   localGoals = item.localGoals,
							   visitorGoals = item.visitorGoals,
							   playDate = item.playDate,
							   monthName = item.monthName,
							   idLocalTeam = item.idLocalTeam,
							   idvisitorTeam = item.idvisitorTeam,
							   idMonth = item.idMonth,
							   year = item.year
						   }
						  ).Take(20).OrderByDescending(x => x.playDate).ToList<MatchResultModel>();

				#region Validation Data
				if (lst.Count == 0)
				{
					Log4NetHelper.GetLog().AddLog(LogLevel.Debug, "not data found for GetAllMatchResults().");
				}
				else
				{
					memoryCache.Add("GetAllMatchResults1", lst, DateTimeOffset.UtcNow.AddMinutes(5));
					Log4NetHelper.GetLog().AddLog(LogLevel.Info, "create caching for method GetAllMatchResults");
					Log4NetHelper.GetLog().AddLog(LogLevel.Info, string.Format("Se encontraron [{0}] Registros ",lst.Count));
				}
				#endregion

				return lst;
			}
		}

		/// <summary>
		/// get alls results the one team 
		/// </summary>
		/// <param name="id">id from team</param>
		/// <returns>List of the objet MatchResultModel </returns>
		public IEnumerable<MatchResultModel> GetAllMatchResultsByTeam(int id)
		{

			Log4NetHelper.GetLog().AddLog(LogLevel.Info, "Call Method GetAllMatchResultsByTeam() .");
			

			var lst = (from item in db.FixturesScores
						   where (item.idLocalTeam == id || item.idvisitorTeam == id)
						   select new MatchResultModel
						   {
							   idFixture = item.idFixture,
							   LocalTeam = item.LocalTeam,
							   VisistorTeam = item.VisistorTeam,
							   localGoals = item.localGoals,
							   visitorGoals = item.visitorGoals,
							   playDate = item.playDate,
							   monthName = item.monthName,
							   idLocalTeam = item.idLocalTeam,
							   idvisitorTeam = item.idvisitorTeam,
							   idMonth = item.idMonth,
							   year = item.year
						   }
						  ).ToList<MatchResultModel>();

				#region Validation 
				if (lst.Count == 0)
				{
					Log4NetHelper.GetLog().AddLog(LogLevel.Debug, "not data found for GetAllMatchResultsByTeam().");
				}
				else
				{
					//memoryCache.Add("GetAllMatchResultsByTeam", lst, DateTimeOffset.UtcNow.AddMinutes(5));
					Log4NetHelper.GetLog().AddLog(LogLevel.Info, "Caching data GetAllMatchResultsByTeam(). ");
				}
				#endregion

				return lst;
			//}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="id">id from team</param>
		/// <param name="year">year the play game </param>
		/// <returns>List of the objet MatchResultModel </returns>
		public IEnumerable<MatchResultModel> GetAllMatchResultsByTeam(int id, int year)
		{

			

			var lst = (from item in db.FixturesScores
					   where ((item.idLocalTeam == id || item.idvisitorTeam == id) && (item.year == year))
					   select new MatchResultModel
					   {
						   idFixture = item.idFixture,
						   LocalTeam = item.LocalTeam,
						   VisistorTeam = item.VisistorTeam,
						   localGoals = item.localGoals,
						   visitorGoals = item.visitorGoals,
						   playDate = item.playDate,
						   monthName = item.monthName,
						   idLocalTeam = item.idLocalTeam,
						   idvisitorTeam = item.idvisitorTeam,
						   idMonth = item.idMonth,
						   year = item.year
					   }
					  ).ToList<MatchResultModel>();

			#region Validation 
			if (lst.Count == 0)
			{
				Log4NetHelper.GetLog().AddLog(LogLevel.Debug, "not data found for GetAllMatchResultsByTeamByYear.");
			}
			else
			{
				//memoryCache.Add("GetAllMatchResultsByTeamByYear", lst, DateTimeOffset.UtcNow.AddMinutes(5));
				Log4NetHelper.GetLog().AddLog(LogLevel.Info, "Caching data GetAllMatchResultsByTeamByYear. ");
			}
			#endregion

			return lst;
			//}

		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="id">id from team</param>
		/// <param name="year">year the play game </param>
		/// <returns>List of the objet MatchResultModel </returns>
		public IEnumerable<MatchResultModel> GetAllMatchResultsByTeam(int id, int year, int month)
		{

			

			var lst = (from item in db.FixturesScores
					   where ((item.idLocalTeam == id || item.idvisitorTeam == id) && (item.year == year) && (item.idMonth == month))
					   select new MatchResultModel
					   {
						   idFixture = item.idFixture,
						   LocalTeam = item.LocalTeam,
						   VisistorTeam = item.VisistorTeam,
						   localGoals = item.localGoals,
						   visitorGoals = item.visitorGoals,
						   playDate = item.playDate,
						   monthName = item.monthName,
						   idLocalTeam = item.idLocalTeam,
						   idvisitorTeam = item.idvisitorTeam,
						   idMonth = item.idMonth,
						   year = item.year
					   }
					  ).ToList<MatchResultModel>();

			#region Validation 
			if (lst.Count == 0)
			{
				Log4NetHelper.GetLog().AddLog(LogLevel.Debug, "not data found for GetAllMatchResultsByTeamByYearByMonth.");
			}
			else
			{
				//memoryCache.Add("GetAllMatchResultsByTeamByYearByMonth", lst, DateTimeOffset.UtcNow.AddMinutes(5));
				Log4NetHelper.GetLog().AddLog(LogLevel.Info, "Caching data GetAllMatchResultsByTeamByYearByMonth. ");
			}
			#endregion

			return lst;
			//}

		}

		#region Private methods 


		#endregion
	}
}
