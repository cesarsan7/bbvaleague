﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace BBVALeague.WepApi.Handler
{
	public class ValidationHandler	: DelegatingHandler
	{  /// <summary>
	   /// Allows REST services  authentication using  Message Handlers
	   /// </summary>
	   /// <param name="request">request uri</param>
	   /// <param name="cancellationToken"></param>
	   /// <returns></returns>
		protected override async Task <HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
		{

			//Seguridad para el API, restricción por dominios permitido
			string domain = System.Configuration.ConfigurationManager.AppSettings["AllowDomain"];
			

			if (!string.Equals(request.RequestUri.Host, domain, StringComparison.InvariantCultureIgnoreCase))
			{
				return request.CreateResponse(HttpStatusCode.Unauthorized);
			}

			var mesagge = await base.SendAsync(request, cancellationToken);

			return mesagge;
		}
	}
}