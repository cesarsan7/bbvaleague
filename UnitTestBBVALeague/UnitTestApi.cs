﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using BBVALeague.Model;
using BBVALeague.WepApi.Controllers;

namespace UnitTestBBVALeague
{
	[TestClass]
	public class UnitTestApi
	{
		//Pendiente revisar .
		//https://msdn.microsoft.com/en-us/library/dn314429(v=vs.113).aspx

		[TestMethod]
		public void TestExistTeam()
		{
			var testEquipos = GetTestTeam();
			var controller = new TeamController();

			
			List<TeamModel> result = new List<TeamModel>(controller.GetTeam());
			Assert.IsTrue(result.Count > 0);

		}


		[TestMethod]
		public void TestMontCount()
		{
			//var testMes = GetTestMes(3);
			var controller = new MonthController();
			List<MonthModel> result = new List<MonthModel>(controller.GetMonths());
			Assert.IsTrue(result.Count == 12);
		}

		[TestMethod]
		public void TestMonthError()
		{
			//var testMes = GetTestMes(3);
			var controller = new MonthController();
			List<MonthModel> result = new List<MonthModel>(controller.GetMonths());
			if (result != null)
			{
				MonthModel item = new MonthModel { idMonth = 7, monthName = "AGOSTO" };
				
				Assert.IsTrue(result.Contains(item));
			}
		}

		[TestMethod]
		public void TestGetLastFiveMatch()
		{
			//var testMes = GetTestMes(3);
			var controller = new MatchResultsController();
			List<MatchResultModel> result = new List<MatchResultModel>(controller.GetLastMatchResults());
			Assert.IsTrue(result.Count > 0 && result.Count <=5);
			Assert.IsFalse(result.Count > 5);

		}


		#region   Private Methods

		private List<TeamModel> GetTestTeam()
		{
			var testEquipo = new List<TeamModel>();
			testEquipo.Add(new TeamModel { idTeam = 2, nameTeam = "Alaves" });


			return testEquipo;
		}
		#endregion
	}
}
