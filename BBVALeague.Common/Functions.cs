﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BBVALeague.Common
{
	public static class Functions
	{


		public static bool IsNumeric(string val)
		{
			Regex _isNumber = new Regex(@"^d+$");
			Match m = _isNumber.Match(val);
			return m.Success;

		}
	}
}
