﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBVALeague.Common
{
	public class Log4NetHelper
	{
		#region Vars

		private ILog log;
		static Log4NetHelper logHelper;

		#endregion  

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		private Log4NetHelper()
		{
			XmlConfigurator.Configure();
			log = LogManager.GetLogger(this.GetType());
		}

		#endregion

		#region Publics

		/// <summary>
		/// Escribe un mensaje de log
		/// </summary>
		/// <param name="level">Nivel del log</param>
		/// <param name="message">Mensaje</param>
		public void AddLog(LogLevel level, string message)
		{
			switch (level)
			{
				case LogLevel.Error:
					log.Error(message);
					break;

				case LogLevel.Info:
					log.Info(message);
					break;

				case LogLevel.Debug:
					log.Debug(message);
					break;

				case LogLevel.Warn:
					log.Warn(message);
					break;
			}
		}

		/// <summary>
		/// Escribe un mensaje de log
		/// </summary>
		/// <param name="level">Nivel del log</param>
		/// <param name="message">Mensaje</param>
		/// <param name="e">Excepcion producida</param>
		public void AddLog(LogLevel level, string message, Exception e)
		{
			switch (level)
			{
				case LogLevel.Error:
					log.Error(message, e);
					break;

				case LogLevel.Info:
					log.Info(message, e);
					break;

				case LogLevel.Debug:
					log.Debug(message, e);
					break;

				case LogLevel.Warn:
					log.Warn(message, e);
					break;
			}
		}

		#endregion

		#region Publics Statics

		/// <summary>
		/// Devuelve la instancia del log
		/// </summary>
		/// <returns></returns>
		public static Log4NetHelper GetLog()
		{
			if (logHelper == null) { logHelper = new Log4NetHelper(); }
			return logHelper;
		}

		#endregion
	}


	/// <summary>
	/// Nivel de log
	/// </summary>
	[Flags]
	public enum LogLevel
	{
		/// <summary>
		/// Indica que no se imprime log
		/// </summary>
		Off = 0,
		/// <summary>
		/// Indica nivel de log en Error
		/// </summary>
		Error = 1,
		/// <summary>
		/// Indica nivel de log en Warn
		/// </summary>
		Warn = 2,
		/// <summary>
		/// Indica nivel de log en Info
		/// </summary>
		Info = 4,
		/// <summary>
		/// Indica nivel de log en Debug
		/// </summary>
		Debug = 8,
		/// <summary>
		/// Indica que se imprime todo, no se debe usar para reportar log
		/// </summary>
		All = 16
	}
}

